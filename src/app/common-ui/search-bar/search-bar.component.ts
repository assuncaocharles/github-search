import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'github-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  searchCtrl = new FormControl('');
  @Output() search = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
    this.searchCtrl.valueChanges.pipe(debounceTime(500)).subscribe((value) => {
      this.search.emit(value);
    });
  }

}
