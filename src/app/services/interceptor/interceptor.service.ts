import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { CONFIG } from '../../../config';

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    const url = new URL(req.url);
    url.searchParams.set('client_id', CONFIG.client_id);
    url.searchParams.set('client_secret', CONFIG.client_secret);
    const dupReq = req.clone({
      url: url.href
    });
    return next.handle(dupReq);
  }
}
