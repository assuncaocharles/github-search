import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { USER_PROFILE_ENDPOINT } from 'src/config';

import { User, UserMetaData } from '..';
import { CONFIG } from '../../../config';

@Component({
  selector: 'github-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {
  @Input() userMeta: UserMetaData;
  user: User;
  githubUrl = CONFIG.github;

  constructor(private dataService: HttpClient) { }

  ngOnInit() {
    this.dataService.get(`${USER_PROFILE_ENDPOINT}${this.userMeta.login}?`).subscribe((user: User) => {
      this.user = user;
    });
  }

}
