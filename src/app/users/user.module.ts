import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';

import { HeaderModule } from '../common-ui/header/header.module';
import { SearchBarModule } from '../common-ui/search-bar/search-bar.module';
import { UserCardModule } from './user-card/user-card.module';
import { UsersContainerComponent } from './users-container/users-container.component';
import { UsersListModule } from './users-list/users-list.module';

@NgModule({
  declarations: [UsersContainerComponent],
  imports: [CommonModule, HttpClientModule, UserCardModule, UsersListModule, SearchBarModule, MatPaginatorModule, HeaderModule],
  exports: [UsersContainerComponent],
  providers: [],
})
export class UsersModule { }
