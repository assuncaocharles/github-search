import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, mergeMap } from 'rxjs/operators';
import { SEARCH_USER_ENDPOINT } from 'src/config';

@Component({
  selector: 'github-users-container',
  templateUrl: './users-container.component.html',
  styleUrls: ['./users-container.component.scss']
})
export class UsersContainerComponent implements OnInit {
  usersResponse;
  pageSize = 30;
  pageIndex = 1;
  getUserSubject = new Subject();
  userSearch = '';
  constructor(private dataService: HttpClient, private router: Router) {

    // @TODO: Isolate all user related requests in a user service
    this.getUserSubject.pipe(
      filter(_ => !!this.userSearch),
      mergeMap((page: string) => {
        this.router.navigate(['.'], {
          queryParams: {
            page,
            user: this.userSearch
          }
        });
        return this.dataService.get(SEARCH_USER_ENDPOINT, {
          params: new HttpParams().set('q', this.userSearch).set('page', page)
        });
      })
    ).subscribe(response => {
      this.usersResponse = response;
    });
  }

  search(searchText) {
    this.userSearch = searchText;
    this.getUserSubject.next(this.pageIndex);
  }

  ngOnInit() {
    this.getUserSubject.next(this.pageIndex);
  }

  nextPage(event: PageEvent) {
    this.getUserSubject.next(event.pageIndex);
  }
}
