import { Component, Input } from '@angular/core';

import { UserMetaData } from '..';

@Component({
  selector: 'github-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent {

  @Input() metaUsers: Array<UserMetaData> = [];

  constructor() { }
}
