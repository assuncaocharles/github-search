import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { UserCardModule } from '../user-card/user-card.module';
import { UsersListComponent } from './users-list.component';

@NgModule({
  declarations: [
    UsersListComponent
  ],
  imports: [
    CommonModule,
    UserCardModule
  ],
  exports: [
    UsersListComponent
  ],
  providers: []
})
export class UsersListModule { }
