export const CONFIG = {
  github: 'https://github.com/',
  base_url: 'https://api.github.com/',
  client_id: '6e449e456ac53f2b1c22',
  client_secret: 'df492535da0ed5cf3198c55cb2e2db428ad49086',
  resources: {
    search: {
      url_append: 'search/',
      sub_resources: {
        users: 'users'
      }
    },
    users: {
      url_append: 'users/'
    }
  }
};



export const SEARCH_USER_ENDPOINT =
  `${CONFIG.base_url}${CONFIG.resources.search.url_append}${CONFIG.resources.search.sub_resources.users}`;
export const USER_PROFILE_ENDPOINT = `${CONFIG.base_url}${CONFIG.resources.users.url_append}`;
